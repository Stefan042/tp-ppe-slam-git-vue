const log = Vue.createApp({
    data(){
        return{

        }
    }
})


log.component('competence', {
    data(){
        return{
            voirCompetence: true
        }
    },
    props: {
        titre: "",
        contenu: ""
    },
    template:`
    <article class="message" v-show="voirCompetence">
        <div class="message-header">
            {{titre}}
            <button type="button" class="close" @click="voirCompetence = false">x</button>
        </div>
        <div class="message-body">
            {{contenu}}
        </div>
    </article>
`

})

log.mount('#root')